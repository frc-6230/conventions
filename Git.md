# Git #

## Project Names ##
project name must be written in `Title Case`.   
The slug must be in `kebab-case`.  

## Branches ##
The main branches are :
* main - main is the last working version.
* dev - dev is the branch for development.

all branches are branched of dev and being called  `dev-[feature name]`.   
The feature name must be in `kebab-case`




## Commits ##
The first commit must be called `Initial Commit`.  


The commit message should be structured as follows:  

```
<type>[optional scope]: <description>

[optional body]
```

### Types ###

Type must be one of the following:
* fix - A bug fix

* feat - A new feature

* docs - Documentation only changes

* style - Changes that do not affect the meaning of the code(white-space, formatting, missing semi-colons, etc)

* refactor - A code change that neither fixes a bug nor adds a feature

### Scopes ###
Scopes are named after the subsystem/command or nothing

example for type and scope :  
* fix[driving]:
* fix:
  
### Description ###
The Description contains a succinct description of the change:
* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize the first letter
* no dot (.) at the end

### Body ###
Just as in the **Description**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.

## Usage ##
* option 1 :
`git commit -m "<type>[optional scope]: <description>" -m "[optional body]" `

* option 2 :
  1. `git commit`
  2. edit opened file
  3. save and close















